# Copyright 2018 AT&T Intellectual Property.  All other rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

DOCKER_REGISTRY            ?= quay.io
IMAGE_NAME                 ?= ranger-agent
IMAGE_PREFIX               ?= attcomdev
HELM                       ?= helm
LABEL                      ?= commit-id
IMAGE_TAG                  ?= latest
COMMIT                     := $(shell git rev-parse HEAD)
PROXY                      ?= http://proxy.foo.com:8000
NO_PROXY                   ?= localhost,127.0.0.1,.svc.cluster.local
USE_PROXY                  ?= false
RANGER_AGENT_USER          := ranger_agent
PUSH_IMAGE                 := false
BASE_IMAGE                 ?= ubuntu:18.04

IMAGE := ${DOCKER_REGISTRY}/${IMAGE_PREFIX}/${IMAGE_NAME}:${IMAGE_TAG}

# Build ranger-agent Docker image for this project
.PHONY: images
images: build_$(IMAGE_NAME)

# Create tgz of the chart
.PHONY: charts
charts: clean
	$(HELM) dep up charts/ranger-agent
	$(HELM) package charts/ranger-agent

# Perform linting
.PHONY: lint
lint: pep8 helm_lint

# Dry run templating of chart
.PHONY: dry-run
dry-run: clean
	tools/helm_tk.sh $(HELM)

# Make targets intended for use by the primary targets above.
.PHONY: build_$(IMAGE_NAME)
build_$(IMAGE_NAME):

ifeq ($(USE_PROXY), true)
	docker build --network host -t $(IMAGE) --label $(LABEL) -f Dockerfile \
                --build-arg user=$(RANGER_AGENT_USER) \
		--build-arg BASE_IMAGE=${BASE_IMAGE} \
		--build-arg http_proxy=$(PROXY) \
		--build-arg https_proxy=$(PROXY) \
		--build-arg HTTP_PROXY=$(PROXY) \
		--build-arg HTTPS_PROXY=$(PROXY) \
		--build-arg no_proxy=$(NO_PROXY) \
		--build-arg NO_PROXY=$(NO_PROXY) .
else
	docker build --network host -t $(IMAGE) --label $(LABEL) -f Dockerfile --build-arg user=$(RANGER_AGENT_USER) --build-arg BASE_IMAGE=${BASE_IMAGE} .
endif

ifeq ($(PUSH_IMAGE), true)
	docker push $(IMAGE)
	docker tag $(IMAGE) ${DOCKER_REGISTRY}/${IMAGE_PREFIX}/${IMAGE_NAME}:$(COMMIT)
	docker push ${DOCKER_REGISTRY}/${IMAGE_PREFIX}/${IMAGE_NAME}:$(COMMIT)
endif

.PHONY: clean
clean:
	rm -rf build
	helm delete helm-template ||:

.PHONY: pep8
pep8:
	tox -e pep8

.PHONY: helm_lint
helm_lint: clean
	tools/helm_tk.sh $(HELM)
	$(HELM) lint charts/ranger-agent
