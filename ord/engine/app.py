# Copyright (c) 2012 OpenStack Foundation
# All Rights Reserved.
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from ord.engine.engine import Engine
from ord.engine.engine import QueueHandler
from ord.openstack.common import log as logging
from ord import service
import oslo_messaging as messaging
import time


LOG = logging.getLogger(__name__)
CONF = service.CONF


def start():
    engine = Engine()

    # start Notify message listener
    transport = messaging.get_rpc_transport(CONF)

    target = messaging.Target(topic='ord-notifier-q',
                              exchange='ranger-agent',
                              server=CONF.DEFAULT.host)

    endpoints = [QueueHandler(engine)]

    server = messaging.get_rpc_server(transport,
                                      target,
                                      endpoints,
                                      executor='eventlet')

    try:
        server.start()
        LOG.info("Messaging engine started")
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        LOG.info("Messaging engine stopped with ctrl-c")

    server.stop()
    server.wait()
