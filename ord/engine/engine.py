#  Copyright 2016 ATT
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

import ast
from multiprocessing import Process

from ord.client import rpcengine
from ord.engine.healthcheck import HealthCheck
from ord.engine.workerfactory import WorkerFactory
from ord.openstack.common import log as logging

LOG = logging.getLogger(__name__)


class QueueHandler(object):

    def __init__(self, engine):
        super(QueueHandler, self).__init__()
        self._engine = engine
        self._rpcengine = rpcengine.RpcEngine()
        self.factory = WorkerFactory()

    def invoke_notifier_rpc(self, ctxt, payload, heat_template):

        LOG.debug("\n----- message from API -----")
        LOG.debug("\n Payload: %s \nctxt: %s "
                  % (str(payload), str(ctxt)))
        LOG.debug("\n-------------------------------\n")
        d = ast.literal_eval(payload)
        template_type = d["template_type"]
        resource_name = d["resource_name"]
        resource_type = d["resource_type"]
        operation = d["resource_operation"]
        template_status_id = d["template_status_id"]
        region = d["region"]
        stack_name = resource_name
        worker = self.factory.getWorker(operation,
                                        stack_name, template_status_id,
                                        resource_type, template_type,
                                        heat_template)
        self.factory.execute(worker, operation)

    def invoke_health_probe_rpc(self, ctxt):
        LOG.debug("\n----- received rabbitmq message from Health-Probe -----")
        LOG.debug("\n Context: %s " % str(ctxt))
        return HealthCheck.execute_health_check()


class Engine(object):
    """This class provides functionality which allows to interact the
    basic ORD clients.
    """

    def __init__(self):
        """Initialize an engine.

        :return: instance of the engine class
        """
        super(Engine, self).__init__()
        # FIXME self.factory = WorkerFactory()

    def _execute(self):
        """Start the process activity."""
        LOG.info("Waiting for a message...")

    def start(self):
        process = Process(target=self._execute)
        try:
            """Start the engine."""
            LOG.info("Starting the engine... (Press CTRL+C to quit)")
            process.start()
            process.join()
        except KeyboardInterrupt:
            process.terminate()
