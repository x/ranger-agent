#  Copyright 2016 ATT
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

from oslo_config import cfg
import oslo_db

from ord.client import heat
from ord.common import exceptions as exc
from ord.db.sqlalchemy import api as db_api
from ord.openstack.common import log as logging


CONF = cfg.CONF
CONF.register_opts([
    cfg.BoolOpt('enable_heat_health_check',
                default=True,
                help='enable health check on heat')
])

LOG = logging.getLogger(__name__)


class HealthCheck(object):

    @classmethod
    def execute_health_check(cls):
        status = {'database': 'unknown',
                  'heat': 'unknown'}

        # Check DB connectivity
        try:
            LOG.debug("Health DB test starting")
            db_api.retrieve_health_record()
            status['database'] = 'passed'

        except oslo_db.exception.DBConnectionError as dbex:
            LOG.error('Health DB Test Exp in %s: %r',
                      cls.__name__, dbex, exc_info=True)
            status['database'] = 'failed'
        except Exception:
            LOG.error('Unknown DB Test Exp in %s',
                      cls.__name__, exc_info=True)
            status['database'] = 'failed'

        # Check Heat connectivity
        if CONF.enable_heat_health_check:
            try:
                LOG.debug("Health Heat test starting")
                heat.HeatClient().delete_stack('non_existant_id')
            except exc.HEATStackDeleteError as heatex:
                if 'MessagingTimeout' in str(heatex):
                    LOG.error('Health Heat Test Exp in %s: %r',
                              cls.__name__, str(heatex), exc_info=True)
                    status['heat'] = 'failed'
                else:
                    status['heat'] = 'passed'
            except Exception:
                LOG.error('Unknown Heat Test Exp in %s',
                          cls.__name__, exc_info=True)
                status['heat'] = 'failed'

        return status
