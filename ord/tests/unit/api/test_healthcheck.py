# Copyright (c) 2019 ATT
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

"""
Unit Tests for ord.api.test_healthcheck
"""
import mock
from mock import patch

from ord.api import healthcheck
from ord.db import api as db_api
from ord.tests import base
import oslo_db


class OrdApiHealthcheckTestCase(base.BaseTestCase):

    def setUp(self):
        super(OrdApiHealthcheckTestCase, self).setUp()
        self.addCleanup(mock.patch.stopall)

    @patch.object(db_api, 'retrieve_health_record', return_value={})
    def test_execute_health_check_success(self, mock_retrieve):
        status = healthcheck.HealthCheck.execute_health_check()
        self.assertEqual(status['database'], 'passed')

    @patch.object(db_api, 'retrieve_health_record',
                  side_effect=oslo_db.exception.DBConnectionError())
    def test_execute_health_check_failure(self, mock_retrieve):
        status = healthcheck.HealthCheck.execute_health_check()

        self.assertEqual(status['database'], 'failed')
