# Copyright (c) 2012 OpenStack Foundation
# All Rights Reserved.
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

"""
Unit Tests for ord.api.test_api
"""

from cgi import FieldStorage
from mox3.mox import stubout
from ord.api.controllers.v1 import api
from ord.db import api as db_api
from ord.tests import base
from oslo_config import cfg
import requests
from unittest import mock
from urllib import request
import webob


CONF = cfg.CONF


class OrdApiTestCase(base.BaseTestCase):

    def setUp(self):
        super(OrdApiTestCase, self).setUp()
        self.stubs = stubout.StubOutForTesting()
        self.addCleanup(self.stubs.UnsetAll)
        self.addCleanup(self.stubs.SmartUnsetAll)

    def test_api_notifier(self):
        kwargs = {
            'request_id': '1',
            'resource_id': 'qwe1234',
            'resource-type': 'image'
        }

        payload = str(kwargs)

        mock_file = FieldStorage('heat_template', headers={})
        params = {
            "file": mock_file,
            "json":
                '{"ord-notifier": {\
                    "request-id": "2",\
                    "resource-id": "1",\
                    "resource-type": "image",\
                    "resource-template-version": "1",\
                    "resource-template-name": "image1",\
                    "resource-template-type": "hot",\
                    "operation": "create",\
                    "region": "local"\
                }\
            }'
        }

        db_response = {'template_type': 'hot',
                       'status': 'Submitted',
                       'resource_name': 'image1',
                       'resource_operation': 'create',
                       'resource_template_version': '1',
                       'request_id': '2', 'region': 'local',
                       'resource_id': '1',
                       'resource_type': 'image',
                       'template_status_id': '1234'}

        CONF.set_default('region', 'local')

        api.rpcapi = mock.MagicMock()
        ord_notifier = api.NotifierController
        ord_notifier._set_keystone_client = mock.MagicMock()
        ord_notifier._validate_token = mock.MagicMock()
        ord_notifier._persist_notification_record = \
            mock.MagicMock(return_value=db_response)

        response = ord_notifier().ord_notifier_POST(**params)

        expect_response = response['ord-notifier-response']['status']
        self.assertEqual(expect_response, 'Submitted')

    def test_api_listener(self):
        ctxt = {'request_id': '1'}
        api_listener = api.ListenerQueueHandler
        kwargs = '{"request_id": "1",'\
                 ' "resource_id": "qwe1234","resource-type": "image"}'
        payload = str(kwargs)
        db_template_target = {'template_type': 'hot',
                              'status': 'STATUS_RDS_SUCCESS',
                              'error_code': '',
                              'error_msg': ''}

        request.urlopen = mock.MagicMock()
        request.Request = mock.MagicMock()
        db_api.update_target_data = mock.MagicMock()
        db_api.retrieve_configuration = mock.MagicMock()
        api_listener().invoke_listener_rpc(ctxt, payload)

    def test_rds_listener_failure(self):
        ctxt = {'request_id': '1'}
        api_listener = api.ListenerQueueHandler

        kwargs = '{"rds-listener": { "ord-notifier-id": "2",'\
                 '"status": "error","resource-type": "image",'\
                 '"error-code": "","error-msg": ""}}'

        db_template_target = {'template_type': 'hot',
                              'status': 'STATUS_RDS_SUCCESS',
                              'error_code': '',
                              'error_msg': ''}

        payload = str(kwargs)
        output_status = 'STATUS_RDS_SUCCESS'
        http_error = requests.exceptions.HTTPError()
        request.urlopen = mock.MagicMock(side_effect=http_error)
        request.Request = mock.MagicMock()
        db_api.update_target_data = mock.MagicMock()
        db_api.retrieve_configuration = mock.MagicMock()

        api_listener().invoke_listener_rpc(ctxt, payload)
        self.assertEqual(output_status, db_template_target['status'])

    def test_rds_listener_success(self):
        ctxt = {'request_id': '1'}
        api_listener = api.ListenerQueueHandler

        kwargs = '{"rds-listener": { "ord-notifier-id": "2",'\
                 '"status": "error","resource-type": "image",'\
                 '"error-code": "","error-msg": ""}}'

        db_template_target = {'template_type': 'hot',
                              'status': 'Error_RDS_Dispatch',
                              'error_code': '',
                              'error_msg': ''}

        payload = str(kwargs)
        output_status = 'Error_RDS_Dispatch'

        request.Request = mock.MagicMock()
        request.urlopen = mock.MagicMock()
        db_api.update_target_data = mock.MagicMock()
        db_api.retrieve_configuration = mock.MagicMock()

        api_listener().invoke_listener_rpc(ctxt, payload)

        self.assertEqual(output_status, db_template_target['status'])

    def test_api_notifier_for_blank_region(self):
        mock_file = FieldStorage('heat_template', headers={})
        params = {
            "file": mock_file,
            "json":
                '{"ord-notifier": {\
                    "request-id": "2",\
                    "resource-id": "1",\
                    "resource-type": "image",\
                    "resource-template-version": "1",\
                    "resource-template-name": "image1",\
                    "resource-template-type": "hot",\
                    "operation": "create"\
                }\
            }'
        }

        ord_notifier = api.NotifierController
        ord_notifier._set_keystone_client = mock.MagicMock()
        ord_notifier._validate_token = mock.MagicMock()

        self.assertRaises(webob.exc.HTTPBadRequest,
                          ord_notifier().ord_notifier_POST,
                          **params)

    def test_api_notifier_for_invalid_region(self):
        mock_file = FieldStorage('heat_template', headers={})
        params = {
            "file": mock_file,
            "json":
                '{"ord-notifier": {\
                    "request-id": "2",\
                    "resource-id": "1",\
                    "resource-type": "image",\
                    "resource-template-version": "1",\
                    "resource-template-name": "image1",\
                    "resource-template-type": "hot",\
                    "operation": "create",\
                    "region": "dev"\
                }\
            }'
        }

        CONF.set_default('region', 'local')

        ord_notifier = api.NotifierController
        ord_notifier._set_keystone_client = mock.MagicMock()
        ord_notifier._validate_token = mock.MagicMock()

        self.assertRaises(webob.exc.HTTPBadRequest,
                          ord_notifier().ord_notifier_POST,
                          **params)

    def test_api_notifier_for_invalid_payload(self):
        mock_file = FieldStorage('heat_template', headers={})
        params = {
            "file": mock_file,
            "json":
                '{"ord-notifier": {\
                    "request-id": "2",\
                    "resource-id": "1",\
                    "resource-type": "image",\
                    "resource-template-version": "1",\
                    "resource-template-name": "ima ge1",\
                    "resource-template-type": "hot",\
                    "operation": "create",\
                    "region": "local"\
                }\
            }'
        }

        CONF.set_default('region', 'local')

        ord_notifier = api.NotifierController
        ord_notifier._set_keystone_client = mock.MagicMock()
        ord_notifier._validate_token = mock.MagicMock()

        self.assertRaises(webob.exc.HTTPBadRequest,
                          ord_notifier().ord_notifier_POST,
                          **params)

    def test_api_ord_notifier_status(self):
        request_id = {"Id": "2"}
        db_template = {'resource_operation': 'create',
                       'resource_id': '1',
                       'region': 'local',
                       'template_type': 'hot',
                       'request_id': '2'}

        db_template_target = {'template_type': 'hot',
                              'status': 'Submitted',
                              'resource_name': 'image1',
                              'resource_operation': 'create',
                              'resource_template_version': '1',
                              'request_id': '2',
                              'region': 'local',
                              'ord-notifier-id': '1',
                              'resource_id': '1',
                              'resource_type': 'image',
                              'template_status_id': '1',
                              'template_version': '1',
                              'error_code': 'ORD_000',
                              'error_msg': 'stack fail'}

        payload = {'rds-listener':
                   {'request-id': '2',
                    'resource-id': '1',
                    'resource-type': 'image',
                    'resource-template-version': '1',
                    'resource-template-type': 'hot',
                    'resource-operation': 'create',
                    'ord-notifier-id': '1',
                    'region': 'local',
                    'status': 'Submitted',
                    'error-code': 'ORD_000',
                    'error-msg': 'stack fail'}
                   }

        ord_notifier = api.NotifierController

        ord_notifier._set_keystone_client = mock.MagicMock()
        db_api.retrieve_template = mock.MagicMock(return_value=db_template)
        db_api.retrieve_target = \
            mock.MagicMock(return_value=db_template_target)

        notification_status = ord_notifier().ord_notifier_status(**request_id)
        self.assertEqual(payload, notification_status)

    def test_update_configuration(self):
        payload = {
            "api_workers": 1,
            "debug_level": "DEBUG",
            "pecan_debug": True,
            "region": "local",
            "resource_creation_timeout_min": 1200,
            "resource_creation_timeout_max": 14400,
            "resource_status_check_wait": 15,
            "api_paste_config": "/etc/ranger-agent/api-paste.ini",
            "transport_url":
            "rabbit://stackrabbit:stackqueue@192.168.56.135:5672",
            "enable_rds_callback_check": True,
            "host": "0.0.0.0",
            "port": 9010,
            "auth_type": "password",
            "auth_url": "http://192.168.56.135/identity/v3",
            "auth_version": "v3",
            "password": "secret",
            "project_domain_name": "Default",
            "project_name": "service",
            "region_name": "RegionOne",
            "user_domain_name": "Default",
            "username": "admin",
            "connection": "mysql+pymysql://root:stackdb@127.0.0.1:3306/ord",
            "max_retries": 5,
            "rds_listener_endpoint": "http://192.168.56.127:8777/v1/rds/status"
        }

        mock_notifierController = api.NotifierController
        mock_notifierController._set_keystone_client = mock.MagicMock()
        mock_notifierController._validate_token = mock.MagicMock()
        db_api.update_configuration = mock.MagicMock()

        resp = mock_notifierController().ord_configuration_update(**payload)

        self.assertEqual(resp, {"Ranger-Agent": "Update request processed"})
