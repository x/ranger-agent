# Copyright (c) 2019 ATT
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

"""
Unit Tests for ord.engine.healthcheck
"""
import mock
from mock import patch

from ord.client import heat
from ord.common import exceptions as exc
from ord.db.sqlalchemy import api as db_api
from ord.engine import healthcheck
from ord.tests import base

import oslo_db


class OrdEngineHealthcheckTestCase(base.BaseTestCase):

    PATH_PREFIX = ''

    def setUp(self):
        super(OrdEngineHealthcheckTestCase, self).setUp()
        self.addCleanup(mock.patch.stopall)

    @patch.object(heat.HeatClient, 'delete_stack', return_value='id')
    @patch.object(heat, 'HeatClient')
    @patch.object(db_api, 'retrieve_health_record', return_value={})
    def test_execute_health_check_db_success(self, mock_retrieve,
                                             mock_heat, mock_delete):
        status = healthcheck.HealthCheck.execute_health_check()

        expected = {'database': 'passed', 'heat': 'unknown'}
        self.assertEqual(status, expected)

    @patch.object(heat.HeatClient, 'delete_stack', return_value='id')
    @patch.object(heat, 'HeatClient')
    @patch.object(db_api, 'retrieve_health_record',
                  side_effect=oslo_db.exception.DBConnectionError())
    def test_execute_health_check_db_failure(self, mock_retrieve,
                                             mock_heat, mock_delete):
        status = healthcheck.HealthCheck.execute_health_check()

        expected = {'database': 'failed', 'heat': 'unknown'}
        self.assertEqual(status, expected)

    @patch.object(heat, 'HeatClient')
    @patch.object(db_api, 'retrieve_health_record', return_value={})
    def test_execute_health_check_heat_success(self, mock_retrieve,
                                               mock_heat):

        mock_delete = mock.MagicMock()
        mock_delete.delete_stack.side_effect = exc.HEATStackDeleteError()
        mock_heat.return_value = mock_delete

        status = healthcheck.HealthCheck.execute_health_check()

        expected = {'database': 'passed', 'heat': 'passed'}
        self.assertEqual(status, expected)

    @patch.object(heat, 'HeatClient')
    @patch.object(db_api, 'retrieve_health_record', return_value={})
    def test_execute_health_check_heat_failure(self, mock_retrieve,
                                               mock_heat):
        mock_delete = mock.MagicMock()
        mock_delete.delete_stack.side_effect = exc.HEATStackDeleteError(
            'MessagingTimeout')
        mock_heat.return_value = mock_delete

        status = healthcheck.HealthCheck.execute_health_check()

        expected = {'database': 'passed', 'heat': 'failed'}
        self.assertEqual(status, expected)
