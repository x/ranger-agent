#!/usr/bin/env python
#
# Copyright (c) 2012 OpenStack Foundation
# All Rights Reserved.
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may
#  not use this file except in compliance with the License. You may obtain
#  a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#  License for the specific language governing permissions and limitations
#  under the License.

import socket
import sys

from oslo_config import cfg
import oslo_i18n as i18n

from ord.common import utils
from ord.i18n import _
from ord.openstack.common import log


LOG = log.getLogger(__name__)

OPTS = [
    cfg.StrOpt('host',
               default=socket.gethostname(),
               help='Name of this node, which must be valid in an AMQP '
               'key. Can be an opaque identifier. For ZeroMQ only, must '
               'be a valid host name, FQDN, or IP address.'),
]
cfg.CONF.register_opts(OPTS, group='DEFAULT')

default_opts = [
    cfg.StrOpt('api_paste_config', default='/etc/ranger-agent/api-paste.ini',
               help=""),
    cfg.IntOpt('api_workers', default=1,
               help="Number of worker threads to be used by API"),
    cfg.StrOpt('debug', default='true',
               help="Enables debug output in logging"),
    cfg.StrOpt('debug_level', default='ERROR',
               help='Determines level of debug content'
               ' output: Error/Warning/Debug'),
    cfg.StrOpt('enable_heat_health_check', default='true', help=""),
    cfg.BoolOpt('pecan_debug', default=True, help=""),
    cfg.StrOpt('region', default='',
               help="name of site ranger-agent is deployed on"),
    cfg.StrOpt('resource_creation_timeout_max', default='14400',
               help="Max allotment of time for resource creation"),
    cfg.StrOpt('resource_creation_timeout_min', default='1200',
               help='Min allotment of time before a timeout'
               ' error can be returned'),
    cfg.StrOpt('resource_status_check_wait', default='15',
               help='Allotment of time between checks'
               ' during resource creation'),
    cfg.IntOpt('retry_limits', default=5,
               help="Max allotment of tries for resource creation"),
    cfg.StrOpt('transport_url', default='',
               help="Messaging queue url", secret=True),
    cfg.StrOpt('glance_api_url', default=None,
               help="glance api internal url"),
    cfg.StrOpt('use_stderr', default='true', help=""),
    cfg.StrOpt('verbose', default='false', help=""),
    cfg.BoolOpt('enable_rds_callback_check',
                default=True,
                help='validate rds api is reachable')
]

auth_opts = [
    cfg.StrOpt('project_name', default='service',
               help="project name  used to stack heat resources"),
    cfg.StrOpt('auth_type', default='password',
               help="type of credentials used for authentication"),
    cfg.StrOpt('auth_url', default='',
               help='auth url used by ranger agent to'
               ' invoke keystone apis'),
    cfg.StrOpt('username', default='',
               help='user name used by ranger agent to'
               ' invoke keystone apis'),
    cfg.StrOpt('password', default='', secret=True,
               help='password used by ranger agent to'
               ' invoke keystone apis'),
    cfg.StrOpt('project_domain_name', default='default',
               help='default project domain '
               'used by ranger agent to invoke keystone apis'),
    cfg.StrOpt('auth_version', default='v3', help="Keystone version"),
    cfg.StrOpt("user_domain_name", default='default',
               help='default project domain '
               'used by ranger agent to invoke keystone apis'),
    cfg.StrOpt("https_cacert", default=None,
               help="Path to CA server certificate for SSL"),

    cfg.StrOpt('region_name', default='', help='Region'),
    cfg.StrOpt('auth_enabled', default='True',
               help='check if authentication turned on')
]

api_opts = [
    cfg.IntOpt('port',
               default=9010,
               help='The port for the ORD API server.',
               ),
    cfg.StrOpt('host',
               default='0.0.0.0',  # nosec
               help='The listen IP for the ORD API server.',
               )

]

orm_opts = [
    cfg.StrOpt('rds_listener_endpoint', default='',
               help='The rds endpoint of ranger deployment'),
    cfg.StrOpt('retry_limits', default='5',
               help='Max attempts to contact Ranger rds endpoint')
]

cfg.CONF.register_opts(default_opts, group='DEFAULT')
cfg.CONF.register_opts(auth_opts, group='keystone_authtoken')
cfg.CONF.register_opts(api_opts, group='api')
cfg.CONF.register_opts(orm_opts, group='orm')

CONF = cfg.CONF


class WorkerException(Exception):
    """Exception for errors relating to service workers."""


def get_workers(name):
    workers = (CONF.DEFAULT.api_workers or utils.cpu_count())
    if workers and workers < 1:
        msg = (_("%(worker_name)s value of %(workers)s is invalid, "
                 "must be greater than 0") %
               {'worker_name': '%s_workers' % name, 'workers': str(workers)})
        raise WorkerException(msg)
    return workers


def prepare_service(argv=None):
    i18n.enable_lazy()
    log_levels = (cfg.CONF.default_log_levels
                  + ['stevedore=INFO'])
    cfg.set_defaults(log.log_opts,
                     default_log_levels=log_levels)
    if argv is None:
        argv = sys.argv
    cfg.CONF(argv[1:], project='ranger-agent', validate_default_values=True)
    log.setup('ranger-agent')
    # messaging.setup()
