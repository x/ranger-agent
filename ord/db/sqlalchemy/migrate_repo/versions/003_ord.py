# Copyright 2012 OpenStack Foundation
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_log import log as logging
from sqlalchemy import Column
from sqlalchemy import dialects
from sqlalchemy import MetaData, String, Table, Boolean
from sqlalchemy import Text

LOG = logging.getLogger(__name__)


# Note on the autoincrement flag: this is defaulted for primary key columns
# of integral type, so is no longer set explicitly in such cases.

def MediumText():
    return Text().with_variant(dialects.mysql.MEDIUMTEXT(), 'mysql')


def upgrade(migrate_engine):
    meta = MetaData()
    meta.bind = migrate_engine

    ord_configuration = Table('ord_configuration', meta,
                              Column('region', String(length=30),
                                     primary_key=True, nullable=False),
                              Column('api_workers', String(length=10),
                                     nullable=False),
                              Column('debug_level', String(length=10),
                                     nullable=False),
                              Column('pecan_debug', Boolean,
                                     nullable=False),
                              Column('resource_creation_timeout_min',
                                     String(length=10), nullable=False),
                              Column('resource_creation_timeout_max',
                                     String(length=10), nullable=False),
                              Column('log_dir', String(length=80),
                                     nullable=False),
                              Column('resource_status_check_wait',
                                     String(length=10), nullable=False),
                              Column('api_paste_config', String(length=80),
                                     nullable=False),
                              Column('transport_url', String(length=300),
                                     nullable=False),
                              Column('enable_rds_callback_check',
                                     Boolean, nullable=False),
                              Column('host', String(length=80),
                                     nullable=False),
                              Column('port', String(length=10),
                                     nullable=False),
                              Column('auth_type', String(length=20),
                                     nullable=False),
                              Column('auth_url', String(length=80),
                                     nullable=False),
                              Column('auth_version', String(length=10),
                                     nullable=False),
                              Column('password', String(length=80),
                                     nullable=False),
                              Column('project_domain_name', String(length=30),
                                     nullable=False),
                              Column('project_name', String(length=80),
                                     nullable=False),
                              Column('region_name', String(length=30),
                                     nullable=False),
                              Column('user_domain_name', String(length=30),
                                     nullable=False),
                              Column('username', String(length=80),
                                     nullable=False),
                              Column('connection', String(length=240),
                                     nullable=False),
                              Column('max_retries', String(length=10),
                                     nullable=False),
                              Column('rds_listener_endpoint',
                                     String(length=120),
                                     nullable=False),
                              mysql_engine='InnoDB',
                              mysql_charset='utf8')

    tables = [ord_configuration]

    for table in tables:
        try:
            table.create()
        except Exception:
            LOG.info(repr(table))
            LOG.exception('Exception while creating table.')
            raise
    if migrate_engine.name == 'mysql':
        # In Folsom we explicitly converted migrate_version to UTF8.
        migrate_engine.execute(
            'ALTER TABLE migrate_version CONVERT TO CHARACTER SET utf8')
        # Set default DB charset to UTF8.
        migrate_engine.execute(
            'ALTER DATABASE %s DEFAULT CHARACTER SET utf8' %
            migrate_engine.url.database)


def downgrade(migrate_engine):
    raise NotImplementedError('Downgrade is not implemented.')
