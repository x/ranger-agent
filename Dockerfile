ARG BASE_IMAGE
FROM ${BASE_IMAGE}

ENV DEBIAN_FRONTEND noninteractive
ENV container docker
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8


RUN apt -qq update && \
apt -y install netcat \
git \
netbase \
openssh-server \
python3-minimal \
python3-setuptools \
python3-pip \
python3-dev \
python3-dateutil \
ca-certificates \
openstack-pkg-tools \
gcc \
g++ \
libffi-dev \
libssl-dev --no-install-recommends \
&& apt-get clean \
&& rm -rf \
     /var/lib/apt/lists/* \
     /tmp/* \
     /var/tmp/* \
     /usr/share/man \
     /usr/share/doc \
     /usr/share/doc-base

RUN pip3 install --upgrade pip
RUN pip3 install wheel

RUN ln -s /usr/bin/python3 /usr/bin/python
COPY . /tmp/ranger-agent

WORKDIR /tmp/ranger-agent

RUN pip3 install --default-timeout=100 -r requirements.txt

RUN python3 setup.py install

ARG user=ranger_agent

# Create user for ranger-agent
RUN useradd -u 1000 -ms /bin/false ${user}

# Change permissions
RUN chown -R ${user}: /home/${user} \
    && chown -R ${user}: /etc/ranger-agent \
    && mkdir /var/log/ranger-agent \
    && chown -R ${user}: /var/log/ranger-agent \
    && cd ~/ \
    && rm -fr /tmp/ranger-agent

# Set work directory
USER ${user}
WORKDIR /home/${user}/
